package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String startDate, String newDate) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			// ServletからuserIdの値が渡ってきていたら整数型に型変換し、idに代入
			Integer id = null;

			if (!StringUtils.isBlank(userId) && userId.matches("^[1-9]+$")) {
				id = Integer.parseInt(userId);
			}

			//絞り込み日付の数値を確認
			//始めの日付
			if (!StringUtils.isBlank(startDate)) {
				startDate += " 00:00:00";
			} else {
				startDate = "2020/01/01 00:00:00";
			}

			//終わりの日付
			if (!StringUtils.isBlank(newDate)) {
				newDate += " 23:59:59";
			} else {
				newDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
			}

			//idがnullなら全件取得、idがnull以外ならその値に対応するユーザーIDの投稿を取得
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, startDate, newDate, id);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message select(int messageId) {

		Connection connection = null;

		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection, messageId);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}

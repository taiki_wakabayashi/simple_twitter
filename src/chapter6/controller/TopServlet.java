package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//投稿の表示
		boolean isShowMessageForm = false;

		//ユーザーコメントインスタンスの初期化
		List<UserComment> comments = null;

		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}


		//ユーザーIDをクリックしたら、ユーザーIDと同じメッセージIDのメッセージだけを表示する
		String userId = request.getParameter("user_id");

		//絞り込む日付を取得
		String startDate = request.getParameter("start");
		String newDate = request.getParameter("end");

		List<UserMessage> messages = new MessageService().select(userId, startDate, newDate);
		comments = new CommentService().select();


		//絞り込み日付の保持
		request.setAttribute("startDate", startDate);
		request.setAttribute("newDate", newDate);

		request.setAttribute("isShowMessageForm", isShowMessageForm);

		//セレクトされたメッセージをセット
		request.setAttribute("messages", messages);

		request.setAttribute("comments", comments);


		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}

}

package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		//エラーの判定
		if (!isValid(request, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		//メッセージのIDを取得
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		Message message = new MessageService().select(messageId);

		request.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		//メッセージのIDを取得
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		//テキストの取得
		String text = request.getParameter("text");

		//メッセージインスタンスの生成
		Message message = new Message();

		//エラーの判定
		if (!isValid(text, errorMessages)) {
			message.setText(text);
			session.setAttribute("message", message);
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		message.setText(text);
		message.setId(messageId);

		new MessageService().update(message);

		session.setAttribute("message", message);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String id = request.getParameter("messageId");

		if (StringUtils.isBlank(id)) {
			errorMessages.add("不正なパラメータが入力されました。");
			return false;
		}

		if (!id.matches("^[0-9]*$")) {
			errorMessages.add("不正なパラメータが入力されました。");
			return false;

		} else if (new MessageService().select(Integer.parseInt(id)) == null) {

			//数字だがメッセージのidにない場合
			errorMessages.add("不正なパラメータが入力されました。");
			return false;
		}

		return true;
	}

}

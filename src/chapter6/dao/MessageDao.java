package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // user_id
			sql.append("    ?, "); // text
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messageId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void update(Connection connection, String text, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET ");
			sql.append("text = ? ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, text);
			ps.setInt(2, messageId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET ");
			sql.append("text = ? ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, message.getText());
			ps.setInt(2, message.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Message select(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messageId);

			ResultSet rs = ps.executeQuery();

			Message message = toSelectMessage(rs);

			if (message.getId() == 0) {
				return null;
			}
			return message;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private Message toSelectMessage(ResultSet rs) throws SQLException {
		Message message = new Message();

		try {
			while (rs.next()) {
				message.setId(rs.getInt("id"));
				message.setUserId(rs.getInt("user_id"));
				message.setText(rs.getString("text"));
				message.setCreatedDate(rs.getDate("created_date"));
				message.setUpdatedDate(rs.getDate("updated_date"));
			}
			return message;
		} finally {
			close(rs);
		}

	}
}
